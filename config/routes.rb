InstantMessageXmppRails::Application.routes.draw do
  
  get "sessions/index"
  get "sessions/logar"
  get "sessions/sair"
  get "sessions/mudarStatus"
  get "sessions/enviarMensagem"
  get "sessions/new"

  post "sessions/index"
  post "sessions/mudarStatus"
  post "sessions/enviarMensagem"

  get  '/login' => 'sessions#index', :as => :login
  post '/login' => 'sessions#logar', :as => :login


  get :online_contacts, to: "sessions#online_contacts"

  root :to => "sessions#index"
  
end
