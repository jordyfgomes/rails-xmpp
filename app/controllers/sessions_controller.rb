#encoding: UTF-8

class SessionsController < ApplicationController
  require 'xmpp4r'
  include Jabber


  attr_reader :client

  #Helpers

 
  helper_method :status
  helper_method :lista
  helper_method :mensagens
  helper_method :statusAtual

  $servidor = "10.0.10.52"
  $dominio = "@optiplex-780"



  def status
    @status
  end

  def lista
    @lista
  end

  def mensagens
    @mensagens
  end

  def statusAtual
    @statusAtual
  end





  def online_contacts
  end

  def index
  end

  def new
    @client = session[:client].inspect
  end

  def logar
    if @client != nil
      @client.close
    end

    @statusAtual = nil
    @lista = Array.new

    session[:username] = params[:username]
    session[:password] = params[:password]

    jid = JID::new("#{session[:username]}#{$dominio}")

    @client = Client::new(jid)
    @client.connect("#{$servidor}")
    @client.auth("#{session[:password]}")
    @client.send(Presence.new.set_type(:available))

    if @client
      @log = "#{@client.jid}, Você está logado no servidor!"
      @posit=0 
      @client.add_presence_callback do |pres|
        @lista[@posit] = pres #cria array com lista de usuário online que acresenta novos usuário automaticamente
        @posit = @posit + 1
        if pres.type  != nil
          n=0
          @lista.each do |e|
            if e.from == pres.from
              @lista.delete_at(n)
            end
            n = n + 1
          end
          @lista.delete(pres)
        end
      end

      #Chama o método callBack
      callBack

      raise "#{@client.attributes}"

      session[:client] = @client.attributes
      # raise "#{@client.as_json}"

      # session[:client] = "nadad"
      render action: "new"
   end

  end

  def sair
    @client.close
    @log = "Deslogado com sucesso"
    redirect_to action: 'index'
    @lista = nil
    @client = nil
  end

  def mudarStatus
    # raise "#{params}"
    if params[:status] == ""
      @statusAtual = nil
      @log = "Status alterado para Disponível"
    else
      @statusAtual = params[:status].to_sym
      @log = "Status alterado para #{params[:status]}"
    end
    # raise "#{Jabber::Presence.new.set_show(@statusAtual)}"

    # nil: online, :chat: free for chat, :away: away from the computer, :dnd: do not disturb, :xa: extended away.

    raise "#{@client.class}"

    @client.send(Jabber::Presence.new.set_show(@statusAtual))

    redirect_to action: 'new'
  end

  def enviarMensagem
    msg = Message::new(params[:to], params[:body])
    msg.type=:normal #:chat
    @client.send(msg)
    @mensagens << "Eu: #{params[:body]} \n"
    redirect_to action: 'new'
  end

  def receberMensagem
    @client.add_message_callback do |m|
      if m.body != nil
          to = m.from.to_s
        @mensagens << "#{to.split('@').first.capitalize}: #{m.body} \n"
      end
    end
  end

  #Chama os métodos callback
  def callBack
    receberMensagem
  end

end